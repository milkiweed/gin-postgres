package main

import (
	"gin-postgresql/db"
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
	"os"
	"time"
)

func main() {
	srv := http.Server{
		Addr:              os.Getenv("HTTP_HOST") + ":" + os.Getenv("HTTP_PORT"),
		IdleTimeout:       3 * time.Second,
		WriteTimeout:      5 * time.Second,
		ReadHeaderTimeout: 10 * time.Second,
		ReadTimeout:       3 * time.Second,
		MaxHeaderBytes:    8192,
		Handler:           router(),
	}

	log.Println("Server starting on http://" + srv.Addr)
	if err := srv.ListenAndServe(); err != nil {
		log.Printf("failure init server %s", err)
		return
	}
}


func router() *gin.Engine {
	r := gin.Default()
	r.GET("/ping", func(c *gin.Context) {
		log.Println(db.DB)
	})
	return r
}