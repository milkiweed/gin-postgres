FROM golang
RUN go get github.com/gin-gonic/gin
RUN go get github.com/golang-migrate/migrate
RUN go get github.com/golang-migrate/migrate/database/postgres
RUN go get github.com/golang-migrate/migrate/source/file
RUN go get github.com/jinzhu/gorm
RUN go get github.com/lib/pq

WORKDIR /go/src/gin-postgresql
