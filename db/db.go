package db

import (
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/lib/pq"
	"log"
	"os"
	"time"
)

var (
	DB  *gorm.DB
	err error
)

func init() {
	for {
		DB, err = gorm.Open("postgres", fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=%s",
			os.Getenv("POSTGRES_HOST"),
			os.Getenv("POSTGRES_PORT"),
			os.Getenv("POSTGRES_USER"),
			os.Getenv("POSTGRES_PASSWORD"),
			os.Getenv("POSTGRES_DB_NAME"),
			os.Getenv("POSTGRES_SSL_MODE"),
		))

		if err != nil {
			log.Println(err)
			time.Sleep(time.Second * 5)
			continue
		}

		DB.SingularTable(true)
		log.Println("DB success started")
		break
	}
}
